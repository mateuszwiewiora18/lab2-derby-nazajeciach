/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trwałość;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Wiwi
 */
@Entity
@Table(name = "JOBS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jobs.findAll", query = "SELECT j FROM Jobs j"),
    @NamedQuery(name = "Jobs.findByJobId", query = "SELECT j FROM Jobs j WHERE j.jobId = :jobId"),
    @NamedQuery(name = "Jobs.findByNameEn", query = "SELECT j FROM Jobs j WHERE j.nameEn = :nameEn"),
    @NamedQuery(name = "Jobs.findByNamePl", query = "SELECT j FROM Jobs j WHERE j.namePl = :namePl"),
    @NamedQuery(name = "Jobs.findByCategory", query = "SELECT j FROM Jobs j WHERE j.category = :category"),
    @NamedQuery(name = "Jobs.findByDescription", query = "SELECT j FROM Jobs j WHERE j.description = :description"),
    @NamedQuery(name = "Jobs.findByIsTreatedAsService", query = "SELECT j FROM Jobs j WHERE j.isTreatedAsService = :isTreatedAsService"),
    @NamedQuery(name = "Jobs.findByIsPhysicalWork", query = "SELECT j FROM Jobs j WHERE j.isPhysicalWork = :isPhysicalWork")})
public class JOBS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "JOB_ID")
    private Integer jobId;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_PL")
    private String namePl;
    @Column(name = "CATEGORY")
    private String category;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "IS_TREATED_AS_SERVICE")
    private Boolean isTreatedAsService;
    @Column(name = "IS_PHYSICAL_WORK")
    private Boolean isPhysicalWork;

    public JOBS() {
    }

    public JOBS(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNamePl() {
        return namePl;
    }

    public void setNamePl(String namePl) {
        this.namePl = namePl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsTreatedAsService() {
        return isTreatedAsService;
    }

    public void setIsTreatedAsService(Boolean isTreatedAsService) {
        this.isTreatedAsService = isTreatedAsService;
    }

    public Boolean getIsPhysicalWork() {
        return isPhysicalWork;
    }

    public void setIsPhysicalWork(Boolean isPhysicalWork) {
        this.isPhysicalWork = isPhysicalWork;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobId != null ? jobId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JOBS)) {
            return false;
        }
        JOBS other = (JOBS) object;
        if ((this.jobId == null && other.jobId != null) || (this.jobId != null && !this.jobId.equals(other.jobId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Trwa\u0142o\u015b\u0107.Jobs[ jobId=" + jobId + " ]";
    }

    public String getNAME_PL() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getNAME_EN() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getJOBS_ID() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getCATEGORY() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getDESCRIPTION() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getis_treated_as_service() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getis_physical_work() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
